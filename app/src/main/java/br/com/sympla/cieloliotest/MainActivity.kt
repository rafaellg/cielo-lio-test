package br.com.sympla.cieloliotest

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import cielo.printer.client.PrinterAttributes
import cielo.sdk.order.PrinterListener
import cielo.sdk.printer.PrinterManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val TAG = MainActivity::class.java.simpleName

    private val printerListener = object : PrinterListener {
        override fun onError(e: Throwable?) {
            Log.e(TAG, "Print Error", e)
            showMessage("Erro na impressão do ingresso")
        }

        override fun onPrintSuccess() {
            Log.d(TAG, "Print Success")
            showMessage("Sucesso")
        }

        override fun onWithoutPaper() {
            Log.d(TAG, "Printer Without Paper")
            showMessage("Impressora sem papel")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button.setOnClickListener { printTest() }
    }

    fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    private fun printTest() {
        val printerManager = PrinterManager(this)

        val printStyle = HashMap<String, Int>()
        printStyle[PrinterAttributes.KEY_ALIGN] = PrinterAttributes.VAL_ALIGN_CENTER

        printerManager.printText(getString(R.string.lorem_ipsum), printStyle, printerListener)
    }
}
